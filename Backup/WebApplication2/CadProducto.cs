﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication2.DSFacturacionTableAdapters;


namespace WebApplication2
{
    public class CadProducto
    {
        private static productoTableAdapter adapter = new productoTableAdapter();


        public static decimal GetPrecio(int codproducto)
        {
            return Convert.ToDecimal(adapter.GetPrecio(codproducto));
        }
    }
}