﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using WebApplication2.DSFacturacionTableAdapters;



namespace WebApplication2
{
    public class CadDetalleFacturaTmp
    {
        private static DetalleFacturaTmpTableAdapter adapter = new DetalleFacturaTmpTableAdapter();

        public static int getTotalCantidad()
         {
             return Convert.ToInt32(adapter.TotalCantidad());
           }

        public static int getTotalValor()
        {
            return Convert.ToInt32(adapter.TotalValor());
        }

        public static string AgregarDetalle(int codproducto, string descripcion, decimal precion, int cantidad)
        {
            int aux = adapter.Insert(codproducto,descripcion,precion,cantidad);
            if(aux==0) return "No se pudo agregar el detalle";
            else return "Detalle Agregado correctamente";
        }

        public static void BorrarTodo()
        {
            adapter.BorrarTodo();
        }

        public static WebApplication2.DSFacturacion.DetalleFacturaTmpDataTable GetDetalle()
        {
            return adapter.GetData();
        }

    
    }
}