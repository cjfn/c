﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NuevaFactura.aspx.cs" Inherits="WebApplication2.NuevaFactura" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 120px;
            text-align: right;
        }
        .style3
        {
            width: 99px;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
    <h2>
        Factura
    </h2>
    <br />
    <table class="style1">
        <tr>
            <td class="style2">
                Fecha:</td>
            <td>
                <asp:TextBox ID="txtFecha" runat="server" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Cliente:</td>
            <td>
                <asp:DropDownList ID="cmbcliente" runat="server" Height="16px" Width="356px" 
                    DataSourceID="SqlDataSource2" DataTextField="nombre" 
                    DataValueField="codcliente">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:facturasConnectionString %>" 
                    SelectCommand="SELECT * FROM [cliente]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Producto:</td>
            <td>
                <asp:DropDownList ID="cmbproducto" runat="server" Height="16px" Width="356px" 
                    DataSourceID="SqlDataSource3" DataTextField="nombre_producto" 
                    DataValueField="codproducto">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:facturasConnectionString %>" 
                    SelectCommand="SELECT * FROM [producto]"></asp:SqlDataSource>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Cantidad</td>
            <td>
                <asp:TextBox ID="txtcantidad" runat="server" TextMode="Number"></asp:TextBox>
&nbsp;<asp:Button ID="btnagregar" runat="server" Text="Agregar" onclick="btnagregar_Click" 
                    style="height: 26px" />
                &nbsp;<asp:Button ID="btnBorrarTodo" runat="server" Text="Borrar Todo"  
                    onclick="btnBorrarTodo_Click" 
                    OnClientClick="return confirm('desea borrar todo el detalle?')" 
                    style="height: 26px"/>
                <asp:Button ID="btnGrabarFactura" runat="server" Text="Grabar Factura" 
                    onclick="btnGrabarFactura_Click" OnClientClick="return confirm('¿esta seguro de grabar la factura?')"/>
            </td>
        </tr>
    </table>
    <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label><br />
    <br />
    <asp:GridView ID="GVDetalle" runat="server" AllowPaging="True" 
        AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="IDLinea" 
        DataSourceID="SqlDataSource1" 
        EmptyDataText="There are no data records to display." 
        style="margin-right: 0px">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            <asp:BoundField DataField="IDLinea" HeaderText="IDLinea" ReadOnly="True" 
                SortExpression="IDLinea" Visible="False" />
            <asp:BoundField DataField="codproducto" HeaderText="codproducto" 
                SortExpression="codproducto" ReadOnly="True" />
            <asp:BoundField DataField="Descripcion" HeaderText="nombre Descripcion" 
                SortExpression="Descripcion" ReadOnly="True" />
            <asp:BoundField DataField="Precio" HeaderText="Precio" 
                SortExpression="Precio" ReadOnly="True" />
            <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" 
                SortExpression="Cantidad" />
                <asp:BoundField DataField="Valor" HeaderText="Valor" 
                SortExpression="Valor" ReadOnly="True" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:facturasConnectionString %>" 
        DeleteCommand="DELETE FROM [DetalleFacturaTmp] WHERE [IDLinea] = @IDLinea" 
        InsertCommand="INSERT INTO [DetalleFacturaTmp] ([codproducto], [Descripcion], [Precio], [Cantidad]) VALUES (@codproducto, @Descripcion, @Precio, @Cantidad)" 
        ProviderName="<%$ ConnectionStrings:facturasConnectionString.ProviderName %>" 
        SelectCommand="SELECT [IDLinea], [codproducto], [Descripcion], [Precio], [Cantidad],  [Precio]*[Cantidad] AS Valor FROM [DetalleFacturaTmp]" 
        UpdateCommand="UPDATE [DetalleFacturaTmp] SET [codproducto] = @codproducto, [Descripcion] = @Descripcion, [Precio] = @Precio, [Cantidad] = @Cantidad WHERE [IDLinea] = @IDLinea">
        <DeleteParameters>
            <asp:Parameter Name="IDLinea" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="codproducto" Type="Int32" />
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="Precio" Type="Decimal" />
            <asp:Parameter Name="Cantidad" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="codproducto" Type="Int32" />
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="Precio" Type="Decimal" />
            <asp:Parameter Name="Cantidad" Type="String" />
            <asp:Parameter Name="IDLinea" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <table class="style1">
        <tr>
            <td>
                Totales</td>
            <td class="style3">
                <asp:TextBox ID="txtTotalCantidad" runat="server" Enabled="False"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtTotalValor" runat="server" Enabled="False"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    <br />
</asp:Content>
