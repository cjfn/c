﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Productos.aspx.cs" Inherits="WebApplication2.Productos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
    </style>
      

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  
  <script>
      $(function () {
          $("#txtbusca").autocomplete({
              source: 'ProductoHandler1.ashx'
          });
         
      });
  </script>

  <h2>PRODUCTOS</h2>
  <p>Crea y administre su inventario
  <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#home">NUEVO</a></li>
    <li><a data-toggle="tab" href="#menu1">BUSCAR</a></li>
    <li><a data-toggle="tab" href="#menu2">MODIFICA</a></li>

  </ul>

  <div class="tab-content">
    <div id="home" class="tab-pane fade in active">
      <h3>CREA PRODUCTOS</h3>
        <table class="auto-style1">
        <tr>
            <td>nombre de producto</td>
            <td>
                <asp:TextBox ID="txtnombre" runat="server" CssClass="form-control"></asp:TextBox></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>cantidad</td>
            <td>
                <asp:TextBox ID="txtcantidad" runat="server" CssClass="form-control"></asp:TextBox></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>precio</td>
            <td>
                <asp:TextBox ID="txtprecio" runat="server" CssClass="form-control"></asp:TextBox></td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>proveedor</td>
            <td>
                <asp:DropDownList ID="ddproveedor" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource2" DataTextField="nombre" DataValueField="codproveedor" CssClass="form-control"></asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:facturacionConnectionString %>" SelectCommand="SELECT * FROM [proveedor]"></asp:SqlDataSource>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
              <asp:Button ID="Button1" runat="server" Text="Registrar" OnClick="Button1_Click" CssClass="btn btn-success" ></asp:Button> </td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>     
    </div>
    <div id="menu1" class="tab-pane fade">
      
                        <center>
                      
                      <asp:TextBox ID="txtbuscaproducto" Visible="false" runat="server" CssClass="btn btn-success"></asp:TextBox>
                             
                        <div class="ui-widget">
                         
                          
               
                        <div class="form-group">
                        <label for="inputPassword" class="col-lg-2 control-label">Busca:</label>
                        <div class="col-lg-10">
                            <input id="txtbusca"  class="form-control" placeholder="Ingresa nombre del producto a buscar">
                            <asp:Button ID="Busca" runat="server" Text="Buscar" CssClass="btn btn-warning"/> 
                            </div>
                        </div>
                        
                        <asp:GridView runat="server" AutoGenerateColumns="False" CellPadding="4" DataSourceID="SqlDataSource3" ForeColor="#333333" GridLines="None">
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                            <Columns>
                                <asp:BoundField DataField="nombre_producto" HeaderText="nombre_producto" SortExpression="nombre_producto" />
                                <asp:BoundField DataField="cantidad" HeaderText="cantidad" SortExpression="cantidad" />
                                <asp:BoundField DataField="precio" HeaderText="precio" SortExpression="precio" />
                                <asp:BoundField DataField="fecha_create" HeaderText="fecha_create" SortExpression="fecha_create" />
                                <asp:BoundField DataField="proveedor" HeaderText="proveedor" SortExpression="proveedor" />
                            </Columns>
                            <EditRowStyle BackColor="#999999" />
                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                          </asp:GridView>
                          <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:facturacionConnectionString %>" SelectCommand="SELECT [nombre_producto], [cantidad], [precio], [fecha_create], [proveedor] FROM [producto] WHERE ([nombre_producto] LIKE '%' + @nombre_producto + '%')">
                              <SelectParameters>
                                  <asp:ControlParameter ControlID="txtbuscaproducto" Name="nombre_producto" PropertyName="Text" Type="String" />
                              </SelectParameters>
                          </asp:SqlDataSource>
                      </center>

    </div>
    <div id="menu2" class="tab-pane fade">
      <h3>MODIFICAR</h3>
   
        <asp:UpdatePanel ID="UpdatePanel1" runat="server"></asp:UpdatePanel>         
      <asp:GridView ID="GridView1" runat="server" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="codproducto" DataSourceID="SqlDataSource1">
          <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
          <Columns>
              <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" ShowSelectButton="True" />
              <asp:BoundField DataField="codproducto" HeaderText="codproducto" InsertVisible="False" ReadOnly="True" SortExpression="codproducto" />
              <asp:BoundField DataField="nombre_producto" HeaderText="nombre_producto" SortExpression="nombre_producto" />
              <asp:BoundField DataField="cantidad" HeaderText="cantidad" SortExpression="cantidad" />
              <asp:BoundField DataField="precio" HeaderText="precio" SortExpression="precio" />
              <asp:BoundField DataField="fecha_create" HeaderText="fecha_create" SortExpression="fecha_create" />
              <asp:BoundField DataField="proveedor" HeaderText="proveedor" SortExpression="proveedor" />
          </Columns>
          <EditRowStyle BackColor="#999999" />
                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#E9E7E2" />
                            <SortedAscendingHeaderStyle BackColor="#506C8C" />
                            <SortedDescendingCellStyle BackColor="#FFFDF8" />
                            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
      </asp:GridView>
      <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:facturacionConnectionString %>" DeleteCommand="DELETE FROM [producto] WHERE [codproducto] = @codproducto" InsertCommand="INSERT INTO [producto] ([nombre_producto], [cantidad], [precio], [fecha_create], [proveedor]) VALUES (@nombre_producto, @cantidad, @precio, @fecha_create, @proveedor)" SelectCommand="SELECT * FROM [producto]" UpdateCommand="UPDATE [producto] SET [nombre_producto] = @nombre_producto, [cantidad] = @cantidad, [precio] = @precio, [fecha_create] = @fecha_create, [proveedor] = @proveedor WHERE [codproducto] = @codproducto">
          <DeleteParameters>
              <asp:Parameter Name="codproducto" Type="Int32" />
          </DeleteParameters>
          <InsertParameters>
              <asp:Parameter Name="nombre_producto" Type="String" />
              <asp:Parameter Name="cantidad" Type="Int32" />
              <asp:Parameter Name="precio" Type="Decimal" />
              <asp:Parameter Name="fecha_create" Type="DateTime" />
              <asp:Parameter Name="proveedor" Type="Int32" />
          </InsertParameters>
          <UpdateParameters>
              <asp:Parameter Name="nombre_producto" Type="String" />
              <asp:Parameter Name="cantidad" Type="Int32" />
              <asp:Parameter Name="precio" Type="Decimal" />
              <asp:Parameter Name="fecha_create" Type="DateTime" />
              <asp:Parameter Name="proveedor" Type="Int32" />
              <asp:Parameter Name="codproducto" Type="Int32" />
          </UpdateParameters>
      </asp:SqlDataSource>
         </div>
    <div id="menu3" class="tab-pane fade">
      <h3>Menu 3</h3>
      <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
    </div>
  </div>



        
        
    
        
  

</asp:Content>
