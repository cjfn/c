﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using WebApplication2.DSFacturaTableAdapters;

namespace WebApplication2.DATOS
{
    public class CadCliente
    {
        private static clienteTableAdapter adapter = new clienteTableAdapter();


        public static string AgregarCliente(string nit, string nombre, string correo)
        {
            int aux = adapter.InsertQuery(nit, nombre, correo);
            if (aux == 0) return "No se pudo agregar el cliente";
            else return "Detalle Agregado correctamente";
        }
    }
}