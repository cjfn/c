﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using WebApplication2.DSFacturaTableAdapters;


namespace WebApplication2
{
    public class CadDetalleFactura
    {
        private static DetalleFacturaTableAdapter adapter = new DetalleFacturaTableAdapter();


        public static string AgregarNuevoDetalle(int codfactura, int codproducto, string descripcion, decimal precio, int cantidad)
        {
            int aux = adapter.InsertQuery(codfactura,codproducto,descripcion,precio, cantidad);
            if (aux == 0) return "No se pudo agregar el detalle";
            else return "Detalle Agregado correctamente";
        }

    }
}