﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication2.DSFacturaTableAdapters;


namespace WebApplication2
{
    public class CadProducto
    {
        private static productoTableAdapter adapter = new productoTableAdapter();


        public static decimal GetPrecio(int codproducto)
        {
            return Convert.ToDecimal(adapter.GetPrecio(codproducto));
        }

        public static  string agregarproducto(string nombre_producto, int cantidad, decimal precio, int proveedor)
        {


            int aux = adapter.insertproducto(nombre_producto, cantidad, precio, proveedor);
            if (aux == 0) return "No se pudo agregar el producto";
            else return "Producto Agregado correctamente";
        }

      
    }
}