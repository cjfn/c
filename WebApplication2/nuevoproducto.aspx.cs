﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class nuevoproducto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            WebApplication2.CadProducto.agregarproducto(txtproducto.Text, Convert.ToInt32(txtcantidad.Text), Convert.ToDecimal(txtprecio.Text), Convert.ToInt32(ddlprovedor.SelectedValue.ToString()));
            txtproducto.Text = "";
            txtprecio.Text = "";
            txtcantidad.Text = "";

            Response.Write("<script>alert('Producto agregado exitosamente');</script>");
            GvProductos.DataBind();
            GvProductos.Visible = true;
        }

       

   
    }
}