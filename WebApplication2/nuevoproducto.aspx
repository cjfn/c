﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="nuevoproducto.aspx.cs" Inherits="WebApplication2.nuevoproducto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table class="auto-style1">
        <tr>
            <td>nombre producto</td>
            <td>
                <asp:TextBox ID="txtproducto" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>cantidad</td>
            <td>
                <asp:TextBox ID="txtcantidad" runat="server" TextMode="Number"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>precio</td>
            <td>
                <asp:TextBox ID="txtprecio" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>proveedor</td>
            <td>
                <asp:DropDownList ID="ddlprovedor" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource1" DataTextField="nombre" DataValueField="codproveedor">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:facturacionConnectionString %>" SelectCommand="SELECT * FROM [proveedor]"></asp:SqlDataSource>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Ingresar" />
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
    <div>
        <center>
        <asp:GridView ID="GvProductos" runat="server" Visible="False" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="codproducto" DataSourceID="SqlDataSource2" ForeColor="#333333" GridLines="None">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                <asp:BoundField DataField="codproducto" HeaderText="codproducto" InsertVisible="False" ReadOnly="True" SortExpression="codproducto" />
                <asp:BoundField DataField="nombre_producto" HeaderText="nombre_producto" SortExpression="nombre_producto" />
                <asp:BoundField DataField="cantidad" HeaderText="cantidad" SortExpression="cantidad" />
                <asp:BoundField DataField="precio" HeaderText="precio" SortExpression="precio" />
                <asp:BoundField DataField="fecha_create" HeaderText="fecha_create" SortExpression="fecha_create" />
                <asp:BoundField DataField="nombre" HeaderText="nombre" SortExpression="nombre" />
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:facturacionConnectionString %>" DeleteCommand="DELETE FROM [producto] WHERE [codproducto] = @codproducto" InsertCommand="INSERT INTO [producto] ([nombre_producto], [cantidad], [precio], [fecha_create], [proveedor]) VALUES (@nombre_producto, @cantidad, @precio, @fecha_create, @proveedor)" SelectCommand="SELECT p.codproducto, p.nombre_producto, p.cantidad, p.precio, p.fecha_create, pr.codproveedor, pr.nombre, p.proveedor FROM [producto] as p join [proveedor] as pr on p.proveedor=pr.codproveedor" UpdateCommand="UPDATE [producto] SET [nombre_producto] = @nombre_producto, [cantidad] = @cantidad, [precio] = @precio, [fecha_create] = @fecha_create, [proveedor] = @proveedor WHERE [codproducto] = @codproducto">
                <DeleteParameters>
                    <asp:Parameter Name="codproducto" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="nombre_producto" Type="String" />
                    <asp:Parameter Name="cantidad" Type="Int32" />
                    <asp:Parameter Name="precio" Type="Decimal" />
                    <asp:Parameter Name="fecha_create" Type="DateTime" />
                    <asp:Parameter Name="nombre" Type="Int32" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="nombre_producto" Type="String" />
                    <asp:Parameter Name="cantidad" Type="Int32" />
                    <asp:Parameter Name="precio" Type="Decimal" />
                    <asp:Parameter Name="fecha_create" Type="DateTime" />
                    <asp:Parameter Name="nombre" Type="String" />
                    <asp:Parameter Name="codproducto" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
        </center>
    </div>
</asp:Content>
