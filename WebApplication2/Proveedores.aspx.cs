﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication2
{
    public partial class Proveedores : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            WebApplication2.DATOS.CadProveedor.AgregarProveedor(txtnombre.Text, txtdireccion.Text, Convert.ToInt32(txttelefono.Text));
            txtnombre.Text = "";
            txtdireccion.Text = "";
            txttelefono.Text = "";
            Response.Write("<script>alert('Proveedor agregado exitosamente');</script>");
        }
    }
}