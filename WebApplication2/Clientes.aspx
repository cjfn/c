﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Clientes.aspx.cs" Inherits="WebApplication2.Clientes" %>


 
 
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <style>
  #draggable { width: 150px; height: 150px; padding: 0.5em; }
  </style>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script> <script>
         $(function () {
             $("#draggable").draggable();
         });
  </script>
    <table class="auto-style1">
        <tr>
            <td>nit</td>
            <td>
                <asp:TextBox ID="txtnit" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>nombre</td>
            <td>
                <asp:TextBox ID="txtnombre" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>correo</td>
            <td>
                <asp:TextBox ID="txtcorreo" runat="server"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>
                <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="ingresar" />
            </td>
        </tr>
    </table>

    <a href="#demo"  data-toggle="collapse in">
    <H2>VER INGRESADOS</H2>
    </a>

     <div id="demo" class="collapse">
         <center>

         <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="codcliente" DataSourceID="SqlDataSource1">
             <Columns>
                 <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                 <asp:BoundField DataField="codcliente" HeaderText="codcliente" InsertVisible="False" ReadOnly="True" SortExpression="codcliente" />
                 <asp:BoundField DataField="nit" HeaderText="nit" SortExpression="nit" />
                 <asp:BoundField DataField="nombre" HeaderText="nombre" SortExpression="nombre" />
                 <asp:BoundField DataField="correo" HeaderText="correo" SortExpression="correo" />
                 <asp:BoundField DataField="fecha_create" HeaderText="fecha_create" SortExpression="fecha_create" />
             </Columns>
         </asp:GridView>
         <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:facturacionConnectionString %>" DeleteCommand="DELETE FROM [cliente] WHERE [codcliente] = @codcliente" InsertCommand="INSERT INTO [cliente] ([nit], [nombre], [correo], [fecha_create]) VALUES (@nit, @nombre, @correo, @fecha_create)" SelectCommand="SELECT * FROM [cliente]" UpdateCommand="UPDATE [cliente] SET [nit] = @nit, [nombre] = @nombre, [correo] = @correo, [fecha_create] = @fecha_create WHERE [codcliente] = @codcliente">
             <DeleteParameters>
                 <asp:Parameter Name="codcliente" Type="Int32" />
             </DeleteParameters>
             <InsertParameters>
                 <asp:Parameter Name="nit" Type="String" />
                 <asp:Parameter Name="nombre" Type="String" />
                 <asp:Parameter Name="correo" Type="String" />
                 <asp:Parameter Name="fecha_create" Type="DateTime" />
             </InsertParameters>
             <UpdateParameters>
                 <asp:Parameter Name="nit" Type="String" />
                 <asp:Parameter Name="nombre" Type="String" />
                 <asp:Parameter Name="correo" Type="String" />
                 <asp:Parameter Name="fecha_create" Type="DateTime" />
                 <asp:Parameter Name="codcliente" Type="Int32" />
             </UpdateParameters>
         </asp:SqlDataSource>
         </center>
     </div>
</asp:Content>
