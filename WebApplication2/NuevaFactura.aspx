﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NuevaFactura.aspx.cs" Inherits="WebApplication2.NuevaFactura" %>

<%@ Register assembly="Microsoft.ReportViewer.WebForms, Version=11.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" namespace="Microsoft.Reporting.WebForms" tagprefix="rsweb" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <style type="text/css">
        .style1
        {
            width: 100%;
        }
        .style2
        {
            width: 120px;
            text-align: right;
        }
        .style3
        {
            width: 99px;
        }
    </style>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
     <script>
         $(function () {
             $("#txtbusca").autocomplete({
                 source: 'ProductoHandler1.ashx'
             });

             $("#txtbuscacliente").autocomplete({
                 source: 'ClientesHandler.ashx'
             });

         });
                      </script>
    <h2>
        Factura
    </h2>
    <br />
    <table class="style1">
        <tr>
            <td class="style2">
                Fecha:</td>
            <td>
                <asp:TextBox ID="txtFecha" runat="server" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td class="style2">
                Cliente:</td>
            <td>
                <asp:DropDownList ID="cmbcliente" runat="server" Height="16px" Width="356px" 
                    DataSourceID="SqlDataSource2" DataTextField="nombre" 
                    DataValueField="codcliente">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource2" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:facturacionConnectionString %>" 
                    SelectCommand="SELECT [codcliente], [nombre] FROM [cliente]"></asp:SqlDataSource>
                <input id="txtbuscacliente"  class="form-control" placeholder="Ingresa nombre o nit del cliente">
                
            </td>
        </tr>
        <tr>
            <td class="style2">
                Producto:</td>
            <td>
                <asp:DropDownList ID="cmbproducto" runat="server" Height="16px" Width="356px" 
                    DataSourceID="SqlDataSource3" DataTextField="nombre_producto" 
                    DataValueField="codproducto">
                </asp:DropDownList>
                <asp:SqlDataSource ID="SqlDataSource3" runat="server" 
                    ConnectionString="<%$ ConnectionStrings:facturacionConnectionString %>" 
                    SelectCommand="SELECT * FROM [producto]"></asp:SqlDataSource>
                
                     
                <input id="txtbusca"  class="form-control" placeholder="Ingresa nombre del producto a buscar">
                
            </td>
        </tr>
        <tr>
            <td class="style2">
                Cantidad</td>
            <td>
                <asp:TextBox ID="txtcantidad" runat="server" TextMode="Number"></asp:TextBox>
&nbsp;<asp:Button ID="btnagregar" runat="server" Text="Agregar" onclick="btnagregar_Click" 
                    style="height: 26px" />
                &nbsp;<asp:Button ID="btnBorrarTodo" runat="server" Text="Borrar Todo"  
                    onclick="btnBorrarTodo_Click" 
                    OnClientClick="return confirm('desea borrar todo el detalle?')" 
                    style="height: 26px"/>
                <asp:Button ID="btnGrabarFactura" runat="server" Text="Grabar Factura" 
                    onclick="btnGrabarFactura_Click" OnClientClick="return confirm('¿esta seguro de grabar la factura?')"/>
            </td>
        </tr>
    </table>
    <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label><br />
    <br />
    <asp:GridView ID="GVDetalle" runat="server" AllowPaging="True" 
        AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="codlinea" 
        DataSourceID="SqlDataSource1" 
        EmptyDataText="There are no data records to display." 
        style="margin-right: 0px">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            <asp:BoundField DataField="codlinea" HeaderText="codlinea" ReadOnly="True" 
                SortExpression="codlinea" Visible="False" />
            <asp:BoundField DataField="codproducto" HeaderText="codproducto" 
                SortExpression="codproducto" ReadOnly="True" />
            <asp:BoundField DataField="Descripcion" HeaderText="nombre Descripcion" 
                SortExpression="Descripcion" ReadOnly="True" />
            <asp:BoundField DataField="Precio" HeaderText="Precio" 
                SortExpression="Precio" ReadOnly="True" />
            <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" 
                SortExpression="Cantidad" />
                <asp:BoundField DataField="Valor" HeaderText="Valor" 
                SortExpression="Valor" ReadOnly="True" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
        ConnectionString="<%$ ConnectionStrings:facturacionConnectionString %>" 
        DeleteCommand="DELETE FROM [DetalleFacturaTmp] WHERE [codlinea] = @codlinea" 
        InsertCommand="INSERT INTO [DetalleFacturaTmp] ([codproducto], [Descripcion], [Precio], [Cantidad]) VALUES (@codproducto, @Descripcion, @Precio, @Cantidad)" 
        SelectCommand="SELECT [codlinea], [codproducto], [Descripcion], [Precio], [Cantidad],  [Precio]*[Cantidad] AS Valor FROM [DetalleFacturaTmp]" 
        UpdateCommand="UPDATE [DetalleFacturaTmp] SET [codproducto] = @codproducto, [Descripcion] = @Descripcion, [Precio] = @Precio, [Cantidad] = @Cantidad WHERE [codlinea] = @codlinea">
        <DeleteParameters>
            <asp:Parameter Name="codlinea" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="codproducto" Type="Int32" />
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="Precio" Type="Decimal" />
            <asp:Parameter Name="Cantidad" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="codproducto" Type="Int32" />
            <asp:Parameter Name="Descripcion" Type="String" />
            <asp:Parameter Name="Precio" Type="Decimal" />
            <asp:Parameter Name="Cantidad" Type="String" />
            <asp:Parameter Name="codlinea" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
    <table class="style1">
        <tr>
            <td>
                Totales</td>
            <td class="style3">
                <asp:TextBox ID="txtTotalCantidad" runat="server" Enabled="False"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtTotalValor" runat="server" Enabled="False"></asp:TextBox>
            </td>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    <br />
    <rsweb:ReportViewer ID="rptViewer" runat="server" Font-Names="Verdana" Font-Size="8pt" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="668px">
        <LocalReport ReportPath="FacturaDetalle.rdlc">
            <DataSources>
                <rsweb:ReportDataSource DataSourceId="ObjectDataSource1" Name="DataSet1" />
            </DataSources>
        </LocalReport>
    </rsweb:ReportViewer>
    <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetData" TypeName="WebApplication2.DSFacturaTableAdapters.select_last_facturaTableAdapter"></asp:ObjectDataSource>
</asp:Content>
