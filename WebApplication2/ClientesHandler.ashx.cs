﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Script.Serialization;

namespace WebApplication2
{
    /// <summary>
    /// Descripción breve de ClientesHandler
    /// </summary>
    public class ClientesHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string term = context.Request["dato"] ?? "";
            List<string> listProduct = new List<string>();
            string cs = ConfigurationManager.ConnectionStrings["facturacionConnectionString"].ConnectionString;
            using (SqlConnection con = new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("getCLientes", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter()
                {
                    ParameterName = "@dato",
                    Value = term
                });
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while (rdr.Read())
                {
                    listProduct.Add((rdr["nit"].ToString()) + " En Inventario: " + (rdr["nombre"].ToString()) + " Precio:" + (rdr["correo"].ToString()));

                }
            }

            JavaScriptSerializer js = new JavaScriptSerializer();
            context.Response.Write(js.Serialize(listProduct));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}