﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Reporting.WebForms;

namespace WebApplication2
{
    public partial class NuevaFactura : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            txtFecha.Text = "" + DateTime.Now;
            ActualizaTotales();
        }

        private void ActualizaTotales()
        {
            txtTotalCantidad.Text = "" + CadDetalleFacturaTmp.getTotalCantidad();
            txtTotalValor.Text = "" + CadDetalleFacturaTmp.getTotalValor();
        }

        protected void btnagregar_Click(object sender, EventArgs e)
        {
            if(txtcantidad.Text=="")
            {
                lblMensaje.Text = "Debe ingresar una cantidad";
                txtcantidad.Focus();
                return;
            }

            int cantidad = Convert.ToInt32(txtcantidad.Text);
            if(cantidad<=0)
            {
                lblMensaje.Text = "debe de ingresar un valor mayor a cero en cantidade";
                 txtcantidad.Focus();
                return;
            }

            int producto = Convert.ToInt32(cmbproducto.SelectedValue.ToString());

            //buscamos el precio del producto
            decimal precio = CadProducto.GetPrecio(producto);

            lblMensaje.Text = CadDetalleFacturaTmp.AgregarDetalle(Convert.ToInt32(cmbproducto.SelectedValue.ToString()), cmbproducto.SelectedItem.ToString(), precio, cantidad);

            GVDetalle.DataBind();
            ActualizaTotales();
             
            

        }

        protected void btnBorrarTodo_Click(object sender, EventArgs e)
        {
            CadDetalleFacturaTmp.BorrarTodo();
            GVDetalle.DataBind();
            ActualizaTotales();
            cmbproducto.Focus();
        }

        protected void btnGrabarFactura_Click(object sender, EventArgs e)
        {

            //validamos que haya detalle
            if(Convert.ToInt32(txtTotalValor.Text)==0)
             
            {
                 lblMensaje.Text = "Debe ingresar detalle para grabar la factura";
                 cmbproducto.Focus();
                 return;
             }

            //obtener el numero de factura

            int numFac = CadFactura.SiguienteFactura();

            //Grabamos encabezado de la factura

            CadFactura.NuevaFactura(Convert.ToInt32(cmbcliente.SelectedValue.ToString()));

            //grabamos el detalle de la factura
            DSFactura.DetalleFacturaTmpDataTable miDetalle = CadDetalleFacturaTmp.GetDetalle();

            foreach (DataRow row in miDetalle)
            {
                

                CadDetalleFactura.AgregarNuevoDetalle(numFac, Convert.ToInt32(row["CodProducto"]), row["Descripcion"].ToString(), Convert.ToDecimal(row["Precio"]), Convert.ToInt32(row["Cantidad"]));

                //borramos la tabla de detallefacturatmp

                CadDetalleFacturaTmp.BorrarTodo();
                GVDetalle.DataBind();
                ActualizaTotales();
                cmbproducto.Focus();

                //mensaje
                lblMensaje.Text = "Factura numero: " + numFac + "grabada con exito";
            }

            ShowReport();



        }

        private void ShowReport()
        {

            int numFac = CadFactura.SiguienteFactura();
            int ultimaFac = CadFactura.UltimaFactura();

            string numero = Convert.ToString(ultimaFac);

           
            //Reset
            rptViewer.Reset();
            //DataSource
            DataSet dts = new DataSet();
            DataTable dt = GetData(ultimaFac);
            ReportDataSource rds = new ReportDataSource("DataSet1", dt);

            rptViewer.LocalReport.DataSources.Add(rds);
            //path
            rptViewer.LocalReport.ReportPath = "FacturaDetalle.rdlc";

            //Parameters
            ReportParameter[] rptParameter = new ReportParameter[]
            {
                new ReportParameter("codfactura",numero)
            };
            rptViewer.LocalReport.SetParameters(rptParameter);
            //Refresh
            rptViewer.LocalReport.Refresh();
        }

          private DataTable GetData(int codfactura)
        {
            DataTable dt = new DataTable();
            string connStr = System.Configuration .ConfigurationManager .ConnectionStrings["facturacionConnectionString"].ConnectionString;
            using (SqlConnection cn = new SqlConnection(connStr))
            {
                SqlCommand cmd = new SqlCommand("select_last_factura", cn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@ultima_factura", SqlDbType.Int).Value = codfactura;

                SqlDataAdapter adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
            }
            return dt;
        }


    }
} 