﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Script.Serialization;

namespace WebApplication2
{
    /// <summary>
    /// Descripción breve de ProductoHandler1
    /// </summary>
    public class ProductoHandler1 : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string term = context.Request["term"]??"";
            List<string> listProduct = new List<string>();
            string cs = ConfigurationManager.ConnectionStrings["facturacionConnectionString"].ConnectionString;
            using(SqlConnection con= new SqlConnection(cs))
            {
                SqlCommand cmd = new SqlCommand("getmiproducto", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter()
                {
                    ParameterName = "@term",
                    Value = term
                });
                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();
                while(rdr.Read())
                {
                    listProduct.Add((rdr["nombre_producto"].ToString()) + " En Inventario: " +(rdr["cantidad"].ToString()) + " Precio:" + (rdr["precio"].ToString()));
 
                }
            }

            JavaScriptSerializer js = new JavaScriptSerializer();
            context.Response.Write(js.Serialize(listProduct));
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}