﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebApplication2.DATOS;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;





namespace WebApplication2
{
    public partial class Productos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


     

        protected void Button1_Click(object sender, EventArgs e)
        {
            string nombre = txtnombre.Text;
            int cantidad = Convert.ToInt32(txtcantidad.Text);
            decimal precio = Convert.ToDecimal(txtprecio.Text);
            int proveedor = Convert.ToInt32(ddproveedor.SelectedValue.ToString());

            CadProducto.agregarproducto(nombre, cantidad, precio, proveedor);
            txtnombre.Text = "";
            txtcantidad.Text = "";
            txtprecio.Text = "";
            
            Response.Write("<script>alert('Producto agregado exitosamente');</script>");
            Response.Redirect("Productos.aspx");
        }

        protected void Busca_Click(object sender, EventArgs e)
        {

        }


        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]

        public static List<string> GetCompletionList(string prefixText, int count)
        {
            return AutoFillProducts(prefixText);

        }

        private static List<string> AutoFillProducts(string prefixText)
        {
            using (SqlConnection con = new SqlConnection())
            {
                con.ConnectionString = ConfigurationManager.ConnectionStrings["facturacionConnectionString"].ConnectionString;
                using (SqlCommand com = new SqlCommand())
                {
                    com.CommandText = "Select nombre_producto,cantidad,precio from producto where" + "nombre_producto like @Search + '%'";
                    com.Parameters.AddWithValue("@Search", prefixText);
                    com.Connection = con;
                    con.Open();
                    List<string> countryNames = new List<string>();
                    using (SqlDataReader sdr = com.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            countryNames.Add(sdr["nombre_producto"].ToString());
                        }
                    }
                    con.Close();
                    return countryNames;
                }
            }
        }  
    }
}